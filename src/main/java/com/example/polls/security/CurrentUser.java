package com.example.polls.security;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import java.lang.annotation.*;

/**
 * Custom annotation to access currently logged in user. It provides access the currently authenticated user in the controllers.
 * The following CurrentUser annotation is a wrapper around @AuthenticationPrincipal annotation.
 */
@AuthenticationPrincipal
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentUser {

}